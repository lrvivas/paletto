
1//05uahXp1dmplPCgYIARAAGAUSNwF-L9IrV5jCBIrZmtWAJXcJZnMPsEha3Ftisn3bhI7tnuharT1ZtJ8BOX6kfQATiR_pU-t3dOs

# paletto

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
